import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {BrowserAnimationsModule, NoopAnimationsModule} from '@angular/platform-browser/animations';
import {MatButtonModule, MatCheckboxModule, MatInputModule, MatFormFieldModule} from '@angular/material';

@NgModule({
  imports: [
    CommonModule, BrowserAnimationsModule, NoopAnimationsModule, MatButtonModule, MatInputModule,
    MatCheckboxModule, MatFormFieldModule
  ],
  exports: [CommonModule, BrowserAnimationsModule, NoopAnimationsModule, MatButtonModule, MatInputModule,
    MatCheckboxModule, MatFormFieldModule]
})
export class MaterialModule { }
