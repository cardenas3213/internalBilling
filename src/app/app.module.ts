import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Component } from '@angular/core';

import { AppComponent } from './app.component';
import {HeaderComponent} from './Components/header/header.component';
import {RouterModule, Routes} from '@angular/router';
import {MaterialModule} from './material/material.module';
import {BillingsComponent} from './Components/Billings/billings/billings.component';
import {AddBillComponent} from './Components/Billings/add-bill/add-bill.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
const appRoutes: Routes = [
  {path: './Components/Billings/billings', component: BillingsComponent }
];

@NgModule({
  declarations: [
    AppComponent, HeaderComponent, AddBillComponent, BillingsComponent

  ],
  imports: [
    BrowserModule, RouterModule.forRoot(appRoutes), MaterialModule, FormsModule, ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
